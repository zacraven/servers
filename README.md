# Servers test task

## How to work

1. Install all dependencies via npm: `npm i`.

2. Execute `npm run s` to run dev-server or `npm run build` to build app for production.
