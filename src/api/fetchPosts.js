import mockedPosts from "./mockedPosts.json"

const fetchPosts = (success = true) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => (success ? resolve(mockedPosts) : reject("Something went wrong!")), 200)
  })
}

export default fetchPosts
