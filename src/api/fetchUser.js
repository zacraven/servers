import mockedUsers from "./mockedUsers.json"

const fetchUser = (nickname, success = true) => {
  const user = mockedUsers.find(u => u.nickname === nickname)

  return new Promise((resolve, reject) => {
    setTimeout(() => (success ? resolve(user) : reject("Something went wrong!")), 200)
  })
}

export default fetchUser
