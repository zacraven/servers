const randomInteger = (min, max) => {
  const rand = min + Math.random() * (max + 1 - min)
  return Math.floor(rand)
}

const makeCancelable = promise => {
  let hasCanceled_ = false

  const wrappedPromise = (...args) => {
    return new Promise((resolve, reject) => {
      promise(...args).then(
        val => (hasCanceled_ ? reject("canceled") : resolve(val)),
        error => (hasCanceled_ ? reject("canceled") : reject(error))
      )
    })
  }

  return {
    promise: wrappedPromise,
    cancel () {
      hasCanceled_ = true
    },
    reset () {
      hasCanceled_ = false
    },
  }
}

export { randomInteger, makeCancelable }
