import React from "react"
import PropTypes from "prop-types"

const Overlay = ({ handleOverlayClick }) => {
  return (
    <div className="overlay" onClick={handleOverlayClick} />
  )
}

Overlay.propTypes = {
  handleOverlayClick: PropTypes.func.isRequired,
}

export default Overlay
