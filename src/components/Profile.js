import React, { Component } from "react"
import PropTypes from "prop-types"

import fetchUser from "API/fetchUser"

import Post from "./Post"
import Spinner from "./Spinner"
import { makeCancelable } from "../utils"

const cancelableFetchUser = makeCancelable(fetchUser)

class Profile extends Component {
  state = {
    user: {},
    loading: false,
    error: null,
  }

  componentDidMount () {
    cancelableFetchUser.reset()
    this.fetchUser()
  }

  componentWillUnmount () {
    cancelableFetchUser.cancel()
  }

  fetchUser = () => {
    this.setState({ loading: true })

    cancelableFetchUser
      .promise(this.props.match.params.nickname)
      .then(user => this.setState({ user, loading: false }))
      .catch(error => {
        if (error === "canceled") return

        this.setState({
          error,
          loading: false,
        })
      })
  }

  render () {
    const { user, loading, error } = this.state
    const { name, surname, nickname, birthdate, about, posts } = user

    if (error) return <div>{error}</div>
    if (loading) return <Spinner />

    return (
      <div className="row">
        <div className="col-12 col-md-4 mb-3">
          <div className="bg-light p-3">
            <h3>{nickname}</h3>
            <h6>{name} {surname}</h6>
            <div className="text-secondary mb-2">{birthdate}</div>
            <div>{about}</div>
          </div>

        </div>
        <div className="col-12 col-md-8">
          {posts && posts.map(post => (
            <Post {...post} key={post.id} />
          ))}
        </div>
      </div>
    )
  }
}

Profile.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      nickname: PropTypes.node,
    }).isRequired,
  }).isRequired,
}

export default Profile
