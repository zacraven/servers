import React from "react"
import PropTypes from "prop-types"
import debounce from "lodash.debounce"

const ActionBar = ({ handleButtonClick, handleSearch }) => {
  const debouncedSearch = debounce(handleSearch, 100)

  return (
    <div className="row justify-content-between">
      <div className="col-md-6 col-12 mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Type to search..."
          onChange={e => debouncedSearch(e.target.value)}
        />
      </div>
      <div className="col-md-4 col-6 mb-3">
        <button type="button" className="btn btn-dark w-100" onClick={handleButtonClick}>
          New post
        </button>
      </div>
    </div>
  )
}

ActionBar.propTypes = {
  handleButtonClick: PropTypes.func.isRequired,
  handleSearch: PropTypes.func.isRequired,
}

export default ActionBar
