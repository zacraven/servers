import React, { Component, Fragment } from "react"

import fetchPosts from "API/fetchPosts"

import PostsList from "./PostsList"
import Spinner from "./Spinner"
import ActionBar from "./ActionBar"
import Modal from "./Modal"
import { randomInteger, makeCancelable } from "../utils"

const cancelableFetchPosts = makeCancelable(fetchPosts)

export default class PostsPage extends Component {
  state = {
    posts: [],
    isModalOpen: false,
    filterBy: "",
    error: null,
    loading: false,
  }

  componentDidMount () {
    cancelableFetchPosts.reset()
    this.fetchPosts()
  }

  componentWillUnmount () {
    cancelableFetchPosts.cancel()
  }

  fetchPosts = () => {
    this.setState({ loading: true })

    const randomErrorChance = !!randomInteger(0, 10) // just for test error functionality
    cancelableFetchPosts
      .promise(randomErrorChance)
      .then(posts => this.setState({ posts, loading: false }))
      .catch(error => {
        if (error === "canceled") return

        this.setState({
          error,
          loading: false,
        })
      })
  }

  openModal = () => this.setState({ isModalOpen: true })

  closeModal = () => this.setState({ isModalOpen: false })

  addPost = post => this.setState(prevState => ({ posts: [post, ...prevState.posts] }))

  handleSearch = filterBy => this.setState({ filterBy })

  render () {
    if (this.state.error) return <div>{this.state.error}</div>
    if (this.state.loading) return <Spinner />

    return (
      <Fragment>
        {this.state.isModalOpen &&
          <Modal handleOverlayClick={this.closeModal} handleSubmit={this.addPost} />}

        <div className="row">
          <div className="col-12">
            <ActionBar handleButtonClick={this.openModal} handleSearch={this.handleSearch} />
            <PostsList posts={this.state.posts} filterBy={this.state.filterBy} />
          </div>
        </div>
      </Fragment>
    )
  }
}
