import React from "react"
import { HashRouter as Router, Route } from "react-router-dom"

import "../styles/main.css"

import Menu from "./Menu"
import PostsPage from "./PostsPage"
import Profile from "./Profile"

const App = () => {
  return (
    <Router>
      <Menu />
      <div className="container">
        <Route exact path="/" component={PostsPage} />
        <Route path="/profile/:nickname" component={Profile} />
      </div>
    </Router>
  )
}

export default App
