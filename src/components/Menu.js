import React from "react"
import { Link, NavLink } from "react-router-dom"

const Menu = () => {
  return (
    <div className="navbar navbar-dark bg-dark navbar-expand-sm mb-4">
      <Link to="/" className="navbar-brand">
        Servers
      </Link>
      <div className="navbar-nav">
        <NavLink exact to="/" className="nav-item nav-link">
          News
        </NavLink>
        {/* In real app url should depends on actual user */}
        <NavLink to="/profile/Batman" className="nav-item nav-link">
          My Profile
        </NavLink>
      </div>
    </div>
  )
}

export default Menu
