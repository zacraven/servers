import React, { Fragment, PureComponent } from "react"
import PropTypes from "prop-types"

import Post from "./Post"

export default class PostsList extends PureComponent {
  get posts () {
    const posts = [ ...this.props.posts ]
    const loweredFilterBy = this.props.filterBy.toLowerCase()

    return posts.filter(p => {
      return p.text.toLowerCase().includes(loweredFilterBy)
    })
  }

  render () {
    const posts = this.posts

    return (
      <Fragment>
        { !posts.length && <div>There are no posts like this.</div> }
        { posts.map(post => (
          <Post {...post} key={post.id} />
        )) }
      </Fragment>
    )
  }
}

PostsList.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object).isRequired,
  filterBy: PropTypes.string.isRequired,
}
