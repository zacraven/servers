import React, { memo } from "react"
import PropTypes from "prop-types"
import { Link } from "react-router-dom"

const Post = ({ author, date, text }) => {
  return (
    <div className="card mb-4" style={{ width: "100%" }}>
      <div className="card-header alert-link">
        <Link to={`/profile/${author}`} className="text-dark">{author}</Link>
      </div>
      <div className="card-body">
        <div className="card-text mb-3">{text}</div>
        <div className="card-text">
          <small className="text-muted">{date}</small>
        </div>
      </div>
    </div>
  )
}

Post.propTypes = {
  author: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
}

export default memo(Post)
