import React, { Fragment, Component } from "react"
import PropTypes from "prop-types"

import { randomInteger } from "../utils"

import Overlay from "./Overlay"
import Error from "./Error"

const MAX_POST_LENGTH = 140

export default class Modal extends Component {
  state = {
    post: "",
    touchedInput: false,
  }

  onChange = ({ target }) => {
    !this.state.touchedInput && this.setState({ touchedInput: true })
    this.setState({ post: target.value })
  }

  onSubmit = () => {
    // HTTP POST to backend URL like /api/:username/posts/
    // Then receiving from backend a new post with additional fields like id, date, etc.

    const { post } = this.state
    const { handleSubmit, handleOverlayClick } = this.props

    const fullPost = {
      id: randomInteger(100, 10000), // sets by server in real app
      author: "Batman", // depends on current user
      date: new Date().toISOString().slice(0, 10), // sets by server in real app
      text: post,
    }

    if (!this.isPostLengthValid(post.length)) return

    handleSubmit(fullPost)
    handleOverlayClick()
  }

  isPostLengthValid = length => length && length <= 140

  render () {
    const { post, touchedInput } = this.state

    return (
      <Fragment>
        <Overlay handleOverlayClick={this.props.handleOverlayClick} />

        <div className="modal-window col-lg-4 col-md-6 col-10 bg-white p-3">
          <div className="form-group mb-4" style={{ position: "relative" }}>
            <label htmlFor="new-post">
              <h4>New post</h4>
            </label>
            <div className="float-right">{MAX_POST_LENGTH - post.length}</div>
            <textarea
              className="form-control"
              id="new-post"
              rows="4"
              value={post}
              onChange={this.onChange}
            />
            {touchedInput && !this.isPostLengthValid(post.length) && <Error>
              Your message must be from 0 to 140 characters length.
            </Error>}
          </div>
          <div className="col-6 mx-auto">
            <button
              type="button"
              className="btn btn-dark w-100"
              onClick={this.onSubmit}
              disabled={!this.isPostLengthValid(post.length)}
            >
              Post
            </button>
          </div>
        </div>
      </Fragment>
    )
  }
}

Modal.propTypes = {
  handleOverlayClick: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
}
