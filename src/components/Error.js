import React from "react"
import PropTypes from "prop-types"

const Error = ({ children }) => {
  return (
    <div className="text-danger error">
      {children}
    </div>
  )
}

Error.propTypes = {
  children: PropTypes.string.isRequired,
}

export default Error
